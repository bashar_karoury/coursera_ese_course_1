/**
 * @file stats.h 
 * @brief This is the header file of stats.c file 
 *
 * @author Bashar Karoury
 *
 */
#ifndef __STATS_H__
#define __STATS_H__


/**
 * @brief print statistics of array to the screen given the lenght
 *
 * A function that prints the statistics of an array including minimum, maximum, mean, and median.
 *
 * @param array : poitner to array of unsigned char
 *
 * @param length : unsigned char length of array
 */
void print_statistics(unsigned char* array, unsigned char length);


/**
 * @brief prints array elements to the screen
 *
 * A function that prints values of element of an array to the scrren 
 *
 * @param array :pointer to array of unsigned char 
 *
 * @param length : unsigned char variable as the length of passed array to be printed  
 *
 * */
void print_array(unsigned char* array, unsigned char length);



/**
 * @brief find the median item in an array
 *   
 *  A function that find the median item  of element of an array
 * 
 *  @param array :pointer to array of unsigned char items 
 *  
 *  @param length : unsigned char variable as the length of passed array
 *  
 *  @return the unsigned char median item
 *
 * */
unsigned char find_median(unsigned char* array, unsigned char length);




/**
*@brief find the mean item in an array 
*
*  A function that find the mean item  of element of an array
*
*  @param array :pointer to array of unsigned char items
*
*  @param length : unsigned char variable as the length of passed array
*
*   @return the unsigned char mean item
*
* */
unsigned char find_mean(unsigned char* array, unsigned char length);



/**
*@brief find the maximum item in an array 
*
* A function that find the maximum item  of element of an array
*
* @param array :pointer to array of unsigned char items
*
* @param length : unsigned char variable as the length of passed array 
*
* @return the unsigned char maximum item
*
* */
unsigned char find_maximum(unsigned char* array, unsigned char length);




/**
*@brief find the minimum item in an array
*
*
* @param array :pointer to array of unsigned char items
*
* @param length : unsigned char variable as the length of passed array
*
* @return the unsigned char minimum item 
*
*/
unsigned char find_minimum(unsigned char* array, unsigned char length);



/*
*@brief sort array descendingly from maximum item to the minimum item
*
* A function to sort the passed array with the maximum item at the 
* first index and the minimum item at the last index
*
* @param array :pointer to array of unsigned char items
*
* @param length : unsigned char variable as the length of passed arra
*
*/ 
void sort_array(unsigned char* array, unsigned char length);





#endif /* __STATS_H__ */
