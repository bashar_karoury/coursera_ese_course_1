/**
 * @file <stats.c>
 *
 * @brief <This files contains implementation of functions declared 
 * in the header file as well as main function to to test functions 
 *
 * @author <Bashar Karoury>
 *
 */



#include <stdio.h>
#include "stats.h"

/* Size of the Data Set */
#define SIZE (40)


/*@brief 
 * function to swap values of two pointers */
void swap(unsigned char* x, unsigned char* y)
{
  unsigned char temp;
  temp = *x;
  *x = *y;
  *y = temp;
}

void main() {

  unsigned char test[SIZE] = { 34, 201, 190, 154,   8, 194,   2,   6,
                              114, 88,   45,  76, 123,  87,  25,  23,
                              200, 122, 150, 90,   92,  87, 177, 244,
                              201,   6,  12,  60,   8,   2,   5,  67,
                                7,  87, 250, 230,  99,   3, 100,  90};
 
 

  //print the statistics of test array 
  
  print_statistics(test, SIZE);

}


void print_statistics(unsigned char* array, unsigned char length)
{

  //first print the array 
   print_array(array, length);

  //print array after sorting
   
   sort_array(array,length);
   printf("Array after sorting :\n");
   print_array(array, length);

   //print the mean 
   unsigned char mean = find_mean(array, length);
   printf("mean item : [%d] \n",mean);

   //print the median 
   unsigned char median = find_median(array, length);
   printf("median item : [%d] \n",median); 

   //print the max 
   unsigned char max = find_maximum(array, length);
   printf("max item : [%d] \n",max);

   //print the min
   unsigned char min = find_minimum(array, length);
   printf("min item : [%d] \n",min);

}


void print_array(unsigned char* array, unsigned char length)
{

 printf("Array[%d] :{", length);
 
 int i;
 for(i=0; i<length; i++)
 {
   printf("%d ",array[i]);
 }

 printf("}\n");
}


unsigned char find_median(unsigned char* array, unsigned char length)
{
  int index_of_median;
  // to find the median item we need to sort the array first 
  sort_array(array, length);

  //now calculate the median index
  if (length % 2)
  {
    index_of_median = (length/2) + 1 ;
  }
  else
  {
    index_of_median = length/2 ;
  }

  return array[index_of_median];

}


unsigned char find_mean(unsigned char* array, unsigned char length)
{
  unsigned int sum = 0;
  int i;
  for(i=0; i< length; i++)
  {
    sum += array[i];
  }

  unsigned char mean = sum/length;
  
  return mean;
}


unsigned char find_maximum(unsigned char* array, unsigned char length)
{
  unsigned char max =  array[0];	// Initially set the max to first item

  int i;
  for(i = 1; i< length; i++)
  {
   if( array[i] > max)
   {
      max = array[i];
   }

  }

  return max;

}


unsigned char find_minimum(unsigned char* array, unsigned char length)
{
 unsigned char min =  array[0];        // Initially set the min to first item
 int i;
 for(i = 1; i< length; i++)
 {
  if( array[i] < min)
    {		           
        min = array[i];
    }
 }
  return min;         

}


void sort_array(unsigned char* array, unsigned char length)
{

 int i = 0;
 int j = 0;
 int index_of_max;
 for(i=0; i<length ; i++)
 {
   index_of_max = i;
   // find the max item in the unsort array
   for(j=i+1; j<length; j++)
   {
     if(array[j] > array[index_of_max])  
     {
       index_of_max = j;                    
     }
   }

   swap(&array[index_of_max], &array[i]);	//Swap the found max with the old one
 }
 

}


